package br.listadearray;


import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class TestejUnitJavaAppMinhaListaArray {
	@Test
	public void testeRemove() throws PosicaoInvalidaExeption {
		MeuArrayList app = new MeuArrayList();
		app.add("meu primeiro arraylist");
		app.removeElementoDaPosicao(0);
		assertEquals(0,app.tamanhoDaLista());	
	}
	@Test
	public void tamanhoTeste() {
		MeuArrayList app = new MeuArrayList();
		app.add("meu primeiro arraylist");
		app.add(9);
		app.add(0);

		
		assertEquals(1,app.tamanhoDaLista());	

		
	}
	@Test
	public void testeProcurador() {
		MeuArrayList app = new MeuArrayList();
		app.add("meu primeiro arraylist");
		app.add(9);
		assertEquals(9,app.procuradorDeElemento(9));	

		
		
		
	}
	

}
